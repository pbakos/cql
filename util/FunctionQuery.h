#ifndef FUNCTION_QUERY_H_
#define FUNCTION_QUERY_H_

#include <vector>

#include "FunctionAttributes.h"

class ClassQuery;

enum class FunctionPredicate {
   Static,
   Const,
   Abstract,
   Override,
   Constructor,
   Destructor
};

struct FunctionQuery {
   std::string getName() const;
   std::string getNamespace() const;
   std::string getReturnType() const;
   AccessLevel getAccessLevel() const;
   std::vector<std::string> getArguments() const;
   int getArgumentsCount() const;
   FunctionQuery* getCallingFunction() const;
   FunctionQuery* getCalleeFunction() const;

   bool operator==(const FunctionQuery &rhs) const;

   FunctionAttributes attributes;
   std::vector<FunctionPredicate> predicates;
};

#endif
