#ifndef ENTITY_QUERY_H_
#define ENTITY_QUERY_H_

#include <string>

struct EntityQuery {
    std::string mName;
    std::string mNamespace;

    virtual ~EntityQuery() {}
};

#endif
