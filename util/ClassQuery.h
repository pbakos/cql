#ifndef CLASS_QUERY_H_
#define CLASS_QUERY_H_

#include <vector>
#include <memory>

#include "EntityQuery.h"

class FunctionQuery;
class VariableQuery;

enum class ClassPredicate {
   Static,
   Abstract
};

struct ClassQuery : public EntityQuery {
   std::unique_ptr<ClassQuery> mInherit;
   std::unique_ptr<ClassQuery> mImplement;

   std::vector<std::unique_ptr<FunctionQuery>> mMembers;
   std::vector<std::unique_ptr<VariableQuery>> mFields;

   std::vector<ClassPredicate> mPredicates;

   ~ClassQuery() = default;
};

#endif
