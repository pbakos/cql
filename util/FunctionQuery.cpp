#include "FunctionQuery.h"

std::string FunctionQuery::getName() const
{
    return attributes.nameAttribute.value();
}

std::string FunctionQuery::getNamespace() const
{
    return attributes.namespaceAttribute.value();
}

std::string FunctionQuery::getReturnType() const
{
    return attributes.returnAttribute.value();
}

AccessLevel FunctionQuery::getAccessLevel() const
{
    return attributes.accessAttribute.value();
}

std::vector<std::string> FunctionQuery::getArguments() const
{
    return attributes.argumentsAttribute.value();
}

int FunctionQuery::getArgumentsCount() const
{
    return attributes.argumentsCountAttribute.value();
}

FunctionQuery* FunctionQuery::getCallingFunction() const
{
    return attributes.callAttribute.value();
}

FunctionQuery* FunctionQuery::getCalleeFunction() const
{
    return attributes.callFromAttribute.value();
}

bool FunctionQuery::operator==(const FunctionQuery &rhs) const
{
    bool isPredicatesEquals = predicates == rhs.predicates;

    bool isAttributesEquals = 
        attributes.nameAttribute == rhs.attributes.nameAttribute &&
        attributes.namespaceAttribute == rhs.attributes.namespaceAttribute &&
        attributes.returnAttribute == rhs.attributes.returnAttribute &&
        attributes.argumentsAttribute == rhs.attributes.argumentsAttribute &&
        attributes.argumentsCountAttribute == rhs.attributes.argumentsCountAttribute;

    return isPredicatesEquals && isAttributesEquals;
}
