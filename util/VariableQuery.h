#ifndef VARIABLE_QUERY_H_
#define VARIABLE_QUERY_H_

#include <memory>

#include "EntityQuery.h"

class ClassQuery;

enum class VariablePredicate {
   Read,
   Write,
   Const,
   Static
};

struct VariableQuery : public EntityQuery {
   std::string mType;
   std::unique_ptr<ClassQuery> mMemberOf;

   std::vector<VariablePredicate> mPredicates;

   ~VariableQuery() = default;
};

#endif
