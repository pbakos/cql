#ifndef VARIABLE_ATTRIBUTES_H
#define VARIABLE_ATTRIBUTES_H

#include <string>
#include <utility>

class ClassQuery;

struct VariableAttributes {

   VariableAttributes() : 
      nameAttribute(std::make_pair("", false)),
      namespaceAttribute(std::make_pair("", false)),
      typeAttribute(std::make_pair("", false)),
      memberOfAttribute(std::make_pair(nullptr, false))
   {}

   std::pair<std::string, bool> nameAttribute;
   std::pair<std::string, bool> namespaceAttribute;
   std::pair<std::string, bool> typeAttribute;
   std::pair<ClassQuery*, bool> memberOfAttribute;
};

#endif
