#ifndef CLASS_ATTRIBUTES_H
#define CLASS_ATTRIBUTES_H

#include <vector>
#include <utility>

#include "ClassQuery.h"
#include "FunctionQuery.h"
#include "VariableQuery.h"

struct ClassAttributes {
   ClassAttributes() : 
      nameAttribute(std::make_pair("", false)),
      namespaceAttribute(std::make_pair("", false)),
      inheritAttribute(std::make_pair(nullptr, false)),
      implementAttribute(std::make_pair(nullptr, false)),
      membersAttribute(std::make_pair(std::vector<FunctionQuery*>(), false)),
      fieldsAttribute(std::make_pair(std::vector<VariableQuery*>(), false))
   {}

   std::pair<std::string, bool> nameAttribute;
   std::pair<std::string, bool> namespaceAttribute;

   std::pair<ClassQuery*, bool> inheritAttribute;
   std::pair<ClassQuery*, bool> implementAttribute;

   std::pair<std::vector<FunctionQuery*>, bool> membersAttribute;
   std::pair<std::vector<VariableQuery*>, bool> fieldsAttribute;
};

#endif
