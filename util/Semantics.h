#ifndef SEMANTICS_H
#define SEMANTICS_H

#include <string>

#include "FunctionQuery.h"
#include "VariableQuery.h"
#include "ClassQuery.h"

#include "ClassAttributes.h"
#include "VariableAttributes.h"
#include "FunctionAttributes.h"

struct Semantics {
    Semantics(int line_, const std::string &name_) : 
        line(line_), name(name_), query(nullptr)
    {
    }

    int line;
    std::string name;
    EntityQuery* query;
};

#endif
