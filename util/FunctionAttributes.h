#ifndef FUNCTION_ATTRIBUTES_H
#define FUNCTION_ATTRIBUTES_H

#include <string>
#include <optional>

class ClassQuery;
class FunctionQuery;

enum class AccessLevel { Private, Protected, Public };

struct FunctionAttributes {
   std::optional<std::string> nameAttribute;
   std::optional<std::string> namespaceAttribute;
   std::optional<AccessLevel> accessAttribute;
   std::optional<std::string> returnAttribute;
   std::optional<int> argumentsCountAttribute;
   std::optional<std::vector<std::string>> argumentsAttribute;
   std::optional<FunctionQuery*> callAttribute;
   std::optional<FunctionQuery*> callFromAttribute;
   std::optional<ClassQuery*> memberOfAttribute;
};

#endif
