%option noyywrap c++ yylineno

%{

#include "Parserbase.h"

%}

%%

"\/\/".*   //comment

"{" return Parser::OPENING_BRACE;
"}" return Parser::CLOSING_BRACE;
"[" return Parser::OPENING_BRACKET;
"]" return Parser::CLOSING_BRACKET;
"(" return Parser::OPENING_PARENTHESIS;
")" return Parser::CLOSING_PARENTHESIS;
"," return Parser::COMMA;
":" return Parser::COLON;
":=" return Parser::ASSIGN;
"\$" return Parser::DOLLAR;

"function" return Parser::FUNCTION;
"class"    return Parser::CLASS;
"variable" return Parser::VARIABLE;

"name"      return Parser::NAME;
"namespace" return Parser::NAMESPACE;
"return"    return Parser::RETURN;
"access"    return Parser::ACCESS;
"private"   return Parser::PRIVATE;
"protected" return Parser::PROTECTED;
"public"    return Parser::PUBLIC;
"arguments" return Parser::ARGUMENTS;
"argumentsCount" return Parser::ARGUMENTS_COUNT;
"memberOf"  return Parser::MEMBER_OF;
"call"      return Parser::CALL;
"callFrom"  return Parser::CALL_FROM;
"type"      return Parser::TYPE;
"inherit"   return Parser::INHERIT;
"implement" return Parser::IMPLEMENT;
"members"   return Parser::MEMBERS;
"fields"    return Parser::FIELDS;

"static"      return Parser::STATIC;
"const"       return Parser::CONST;
"override"    return Parser::OVERRIDE;
"abstract"    return Parser::ABSTRACT;
"constructor" return Parser::CONSTRUCTOR;
"destructor"  return Parser::DESTRUCTOR;
"read"        return Parser::READ;
"write"       return Parser::WRITE;

[[:digit:]]+              return Parser::NUMBER;
[_[:alpha:]][_[:alnum:]]* return Parser::IDENTIFIER;


[[:blank:][:space:]]+      //nothing to do

%%
