#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>

#include "Parser.h"

template <typename T>
std::string testValues(T leftValue, T rightValue)
{
    if (leftValue == rightValue) {
        return "PASSED";
    } else {
        return "FAILED";
    }
}

void testFunctionQueryAttributes(const std::string& testFileName)
{
    std::ifstream testFile(testFileName.c_str());

    Parser parser(testFile);
    parser.parse();

    auto functionQuery = parser.getFunctionQueries().back();
    
    std::ostringstream testOutput;
    testOutput << "\nFunction query attributes test\n";
    testOutput << "------------------------------\n";
    
    testOutput << std::setw(15) << std::left << "Name" << '|';
    testOutput << std::setw(15) << std::right << testValues(functionQuery->getName(), std::string("f")) << '\n';

    testOutput << std::setw(15) << std::left << "Namespace" << '|';
    testOutput << std::setw(15) << std::right << testValues(functionQuery->getNamespace(), std::string("ns")) << '\n';

    testOutput << std::setw(15) << std::left << "Return" << '|';
    testOutput << std::setw(15) << std::right << testValues(functionQuery->getReturnType(), std::string("int")) << '\n';

    testOutput << std::setw(15) << std::left << "Access" << '|';
    testOutput << std::setw(15) << std::right << testValues(functionQuery->getAccessLevel(), AccessLevel::Public) << '\n';

    std::vector<std::string> argumentsMock{ "int", "float" };
    testOutput << std::setw(15) << std::left << "Arguments" << '|';
    testOutput << std::setw(15) << std::right << testValues(functionQuery->getArguments(), argumentsMock) << '\n';

    testOutput << std::setw(15) << std::left << "Arguments count" << '|';
    testOutput << std::setw(15) << std::right << testValues(functionQuery->getArgumentsCount(), 2) << '\n';

    std::unique_ptr<FunctionQuery> callFunctionMock = std::make_unique<FunctionQuery>();
    callFunctionMock->attributes.nameAttribute = "g";
    testOutput << std::setw(15) << std::left << "Call" << '|';
    testOutput << std::setw(15) << std::right << testValues(*functionQuery->getCallingFunction(), *callFunctionMock.get()) << '\n';

    std::unique_ptr<FunctionQuery> callFromFunctionMock = std::make_unique<FunctionQuery>();
    callFromFunctionMock->attributes.nameAttribute = "h";
    testOutput << std::setw(15) << std::left << "Call from" << '|';
    testOutput << std::setw(15) << std::right << testValues(*functionQuery->getCalleeFunction(), *callFromFunctionMock.get()) << '\n';

    std::cout << testOutput.str() << '\n';
}

int main()
{
    testFunctionQueryAttributes("test/function_attributes.cql");
}
