%baseclass-preinclude "util/Semantics.h"
%lsp-needed

%polymorphic
    String : std::string;
    StringList : std::vector<std::string>;
    Int : int;

    EntityQuery : EntityQuery*;
    ClassQuery : ClassQuery*;
    VariableQuery : VariableQuery*;
    FunctionQuery : FunctionQuery*;

    ClassPredicates : std::vector<ClassPredicate>;
    VariablePredicates: std::vector<VariablePredicate>;
    FunctionPredicates: std::vector<FunctionPredicate>;

    ClassAttributes : ClassAttributes*;
    VariableAttributes : VariableAttributes*;
    FunctionAttributes : FunctionAttributes*;

    VariableList : std::vector<VariableQuery*>;
    FunctionList : std::vector<FunctionQuery*>;
    AccessLevel : AccessLevel;

%token

OPENING_BRACE
CLOSING_BRACE
OPENING_BRACKET
CLOSING_BRACKET
OPENING_PARENTHESIS
CLOSING_PARENTHESIS
COMMA
COLON
ASSIGN
DOLLAR

FUNCTION
CLASS
VARIABLE

NAME
NAMESPACE
RETURN
ACCESS
PRIVATE
PROTECTED
PUBLIC
ARGUMENTS
ARGUMENTS_COUNT
MEMBER_OF
CALL
CALL_FROM
TYPE
INHERIT
IMPLEMENT
MEMBERS
FIELDS

STATIC
CONST
OVERRIDE
ABSTRACT
CONSTRUCTOR
DESTRUCTOR
READ
WRITE

IDENTIFIER
NUMBER

%type <EntityQuery> query;
%type <ClassQuery> class_query;
%type <VariableQuery> variable_query;
%type <FunctionQuery> function_query;

%type <String> name_attribute;
%type <String> namespace_attribute;
%type <String> attribute_value;

%type <FunctionAttributes> function_attributes;
%type <AccessLevel> access_attribute;
%type <String> return_attribute;
%type <Int> arguments_count_attribute;
%type <FunctionQuery> call_attribute;
%type <FunctionQuery> call_from_attribute;
%type <StringList> arguments_attribute;
%type <ClassQuery> member_of_attribute;
%type <FunctionPredicates> function_predicates;

%type <ClassAttributes> class_attributes;
%type <ClassQuery> inherit_attribute;
%type <ClassQuery> implement_attribute;
%type <ClassPredicates> class_predicates;
%type <VariableList> fields_attribute;
%type <VariableList> variable_list;
%type <VariableList> variable_list_elements;
%type <FunctionList> members_attribute;
%type <FunctionList> function_list;

%type <VariableAttributes> variable_attributes;
%type <String> type_attribute;
%type <VariablePredicates> variable_predicates;

%type <StringList> identifier_list;

%%

start:
   query
   {
      queries.emplace_back(d_loc__.first_line, "");
      queries.back().query = $1;
   }
|
   function_query
   {
      functionQueries.push_back($1);
   }
|
   IDENTIFIER ASSIGN query
   {
      queries.emplace_back(d_loc__.first_line, mLastIdentifier);
      queries.back().query = $3;
   }
|
   IDENTIFIER OPENING_PARENTHESIS predicate_arguments_elements CLOSING_PARENTHESIS ASSIGN query
;

predicate_arguments_elements:
   IDENTIFIER
|
   predicate_arguments_elements COMMA IDENTIFIER
;

query:
   class_query
   {
      $$ = $1;
   }
|
   variable_query
   {
      $$ = $1;
   }  
;

function_query:
   FUNCTION OPENING_BRACE function_attributes function_predicates CLOSING_BRACE
   {
      $$ = new FunctionQuery;
      $$->attributes = *($3);
      $$->predicates = $4;

      delete $3;
   }
;

attribute_value:
   IDENTIFIER
   {
      $$ = mLastIdentifier;
   }
|
   DOLLAR IDENTIFIER
   {
      $$ = mLastIdentifier; //todo
   }
;

function_attributes:
   name_attribute function_attributes
   {
      $$ = $2;
      $$->nameAttribute = $1;
   }
|
   namespace_attribute function_attributes
   {
      $$ = $2;
      $$->namespaceAttribute = $1;
   }
|
   access_attribute function_attributes
   {
      $$ = $2;
      $$->accessAttribute = $1;
   }
|
   return_attribute function_attributes
   {
      $$ = $2;
      $$->returnAttribute = $1;
   }
|
   arguments_count_attribute function_attributes
   {
      $$ = $2;
      $$->argumentsCountAttribute = $1;
   }
|
   arguments_attribute function_attributes
   {
      $$ = $2;
      $$->argumentsAttribute = $1;
   }
|
   call_attribute function_attributes
   {
      $$ = $2;
      $$->callAttribute = $1;
   }
|
   call_from_attribute function_attributes
   {
      $$ = $2;
      $$->callFromAttribute = $1;
   }
|
   member_of_attribute function_attributes
   {
      $$ = $2;
      $$->memberOfAttribute = $1;
   }
|
   //epsilon
   {
      $$ = new FunctionAttributes;
   }
;

name_attribute:
   NAME COLON attribute_value
   {
      $$ = $3;
   }
;

namespace_attribute:
   NAMESPACE COLON attribute_value
   {
      $$ = $3;
   }
;

access_attribute:
   ACCESS COLON PRIVATE
   {
      $$ = AccessLevel::Private;
   }
|
   ACCESS COLON PROTECTED
   {
      $$ = AccessLevel::Protected;
   }
|
   ACCESS COLON PUBLIC
   {
      $$ = AccessLevel::Public;
   }
;

return_attribute:
   RETURN COLON attribute_value
   {
      $$ = $3;
   }
;

arguments_count_attribute:
   ARGUMENTS_COUNT COLON NUMBER
   {
      $$ = mLastNumber;
   }
;

arguments_attribute:
   ARGUMENTS COLON OPENING_BRACKET CLOSING_BRACKET
   {
      $$ = std::vector<std::string>();
   }
|
   ARGUMENTS COLON OPENING_BRACKET identifier_list CLOSING_BRACKET
   {
      $$ = $4;
      std::reverse(std::begin($$), std::end($$));
   }
;

identifier_list:
   attribute_value
   {
      $$ = std::vector<std::string>{$1};
   }
|
   attribute_value COMMA identifier_list
   {
      $$ = std::move($3);
      ($$).push_back($1);
   }
;

call_attribute:
   CALL COLON function_query
   {
      $$ = $3;
   }
;

call_from_attribute:
   CALL_FROM COLON function_query
   {
      $$ = $3;
   }
;

member_of_attribute:
   MEMBER_OF COLON class_query
   {
      $$ = $3;
   }
;

function_predicates:
   CONST function_predicates
   {
      ($$).insert(std::end($$), std::begin($2), std::end($2));
      ($$).push_back(FunctionPredicate::Const);
   }
|
   STATIC function_predicates
   {
      ($$).insert(std::end($$), std::begin($2), std::end($2));
      ($$).push_back(FunctionPredicate::Static);
   }
|
   ABSTRACT function_predicates
   {
      ($$).insert(std::end($$), std::begin($2), std::end($2));
      ($$).push_back(FunctionPredicate::Abstract);
   }
|
   OVERRIDE function_predicates
   {
      ($$).insert(std::end($$), std::begin($2), std::end($2));
      ($$).push_back(FunctionPredicate::Override);
   }
|
   CONSTRUCTOR function_predicates
   {
      ($$).insert(std::end($$), std::begin($2), std::end($2));
      ($$).push_back(FunctionPredicate::Constructor);
   }
|
   DESTRUCTOR function_predicates
   {
      ($$).insert(std::end($$), std::begin($2), std::end($2));
      ($$).push_back(FunctionPredicate::Destructor);
   }
|
   //epsilon
   {
      $$ = std::vector<FunctionPredicate>();
   }
;

class_query:
   CLASS OPENING_BRACE class_attributes class_predicates CLOSING_BRACE
   {
      $$ = new ClassQuery;

      if ($3->nameAttribute.second) {
         $$->mName = $3->nameAttribute.first;
      }

      if ($3->namespaceAttribute.second) {
         $$->mNamespace = $3->namespaceAttribute.first;
      }

      if ($3->inheritAttribute.second) {
         $$->mInherit = std::unique_ptr<ClassQuery>($3->inheritAttribute.first);
      }

      if ($3->implementAttribute.second) {
         $$->mImplement = std::unique_ptr<ClassQuery>($3->implementAttribute.first);
      }

      if ($3->fieldsAttribute.second) {
         auto makeUniqueVariableQuery = [](VariableQuery* variableQuery) { 
             return std::unique_ptr<VariableQuery>(variableQuery); 
         };
     
         std::transform(std::begin($3->fieldsAttribute.first), std::end($3->fieldsAttribute.first),
            std::back_inserter($$->mFields), makeUniqueVariableQuery);
      }

      $$->mPredicates = $4;

      delete $3;
   }
;

class_attributes:
   name_attribute class_attributes
   {
      $$ = $2;
      $$->nameAttribute.first = $1;
      $$->nameAttribute.second = true;
   }
|
   namespace_attribute class_attributes
   {
      $$ = $2;
      $$->namespaceAttribute.first = $1;
      $$->namespaceAttribute.second = true;
   }
|
   inherit_attribute class_attributes
   {
      $$ = $2;
      $$->inheritAttribute.first = $1;
      $$->inheritAttribute.second = true;
   }
|
   implement_attribute class_attributes
   {
      $$ = $2;
      $$->implementAttribute.first = $1;
      $$->implementAttribute.second = true;
   }
|
   fields_attribute class_attributes
   {
      $$ = $2;
      $$->fieldsAttribute.first = $1;
      $$->fieldsAttribute.second = true;
   }
|
   members_attribute class_attributes
   {
      $$ = $2;
      $$->membersAttribute.first = $1;
      $$->membersAttribute.second = true;
   }
|
   //epsilon
   {
      $$ = new ClassAttributes;
   }
;

inherit_attribute:
   INHERIT COLON class_query
   {
      $$ = $3;
   }
;

implement_attribute:
   IMPLEMENT COLON class_query
   {
      $$ = $3;
   }
;

fields_attribute:
   FIELDS COLON variable_list
   {
      $$ = $3;
   }
;

variable_list:
   OPENING_BRACKET variable_list_elements CLOSING_BRACKET
   {
      $$ = $2;
   }
|
   OPENING_BRACKET CLOSING_BRACKET
   {
      $$ = std::vector<VariableQuery*>();
   }
;

variable_list_elements:
   variable_query
   {
      ($$).push_back($1);
   }
|
   variable_list_elements COMMA variable_query
   {
      ($$).insert(std::end($$), std::begin($1), std::end($1));
      ($$).push_back($3);
   }
;

members_attribute:
   MEMBERS COLON OPENING_BRACKET CLOSING_BRACKET
   {
      $$ = std::vector<FunctionQuery*>();
   }
|
   MEMBERS COLON OPENING_BRACKET function_list CLOSING_BRACKET
   {
      $$ = $4;
   }
;

function_list:
   function_query
   {
      ($$).push_back($1);
   }
|
   function_list COMMA function_query
   {
      $$ = std::move($1);
      ($$).push_back($3);
   }
;

class_predicates:
   ABSTRACT class_predicates
   {
      ($$).insert(std::end($$), std::begin($2), std::end($2));
      ($$).push_back(ClassPredicate::Abstract);
   }
|
   STATIC class_predicates
   {
      ($$).insert(std::end($$), std::begin($2), std::end($2));
      ($$).push_back(ClassPredicate::Static);
   }
|
   //epsilon
   {
      $$ = std::vector<ClassPredicate>();
   }
;

variable_query:
   VARIABLE OPENING_BRACE variable_attributes variable_predicates CLOSING_BRACE
   {
      $$ = new VariableQuery;

      if ($3->nameAttribute.second) {
         $$->mName = $3->nameAttribute.first;
      }

      if ($3->namespaceAttribute.second) {
         $$->mNamespace = $3->namespaceAttribute.first;
      }

      if ($3->typeAttribute.second) {
         $$->mType = $3->typeAttribute.first;
      }

      if ($3->memberOfAttribute.second) {
         $$->mMemberOf = std::unique_ptr<ClassQuery>($3->memberOfAttribute.first);
      }

      $$->mPredicates = $4;

      delete $3;
   }
;

variable_attributes:
   name_attribute variable_attributes
   {
      $$ = $2;
      $$->nameAttribute.first = $1;
      $$->nameAttribute.second = true;
   }
|
   namespace_attribute variable_attributes
   {
      $$ = $2;
      $$->namespaceAttribute.first = $1;
      $$->namespaceAttribute.second = true;
   }
|
   type_attribute variable_attributes
   {
      $$ = $2;
      $$->typeAttribute.first = $1;
      $$->typeAttribute.second = true;
   }
|
   member_of_attribute variable_attributes
   {
      $$ = $2;
      $$->memberOfAttribute.first = $1;
      $$->memberOfAttribute.second = true;
   }
|
   //epsilon
   {
      $$ = new VariableAttributes;
   }
;

type_attribute:
   TYPE COLON attribute_value
   {
      $$ = $3;
   }
;

variable_predicates:
   READ variable_predicates
   {
      ($$).insert(std::end($$), std::begin($2), std::end($2));
      ($$).push_back(VariablePredicate::Read);
   }
|
   WRITE variable_predicates
   {
      ($$).insert(std::end($$), std::begin($2), std::end($2));
      ($$).push_back(VariablePredicate::Write);
   }
|
   CONST variable_predicates
   {
      ($$).insert(std::end($$), std::begin($2), std::end($2));
      ($$).push_back(VariablePredicate::Const);
   }
|
   STATIC variable_predicates
   {
      ($$).insert(std::end($$), std::begin($2), std::end($2));
      ($$).push_back(VariablePredicate::Static);
   }
|
   //epsilon
   {
      $$ = std::vector<VariablePredicate>();
   }
;
